import pytest
from flask import url_for, request
from app import create_app

@pytest.fixture
def app():
    app = create_app()
    return app

@pytest.mark.usefixtures('client_class')
class TestLiveServer:

    def test_html(self):
        rv = self.client.get(url_for('index'))
        assert b'submit.' in rv.data
        assert rv.status_code == 200

    def test_post(self):
        rv = self.client.post('/hi', data=dict(text="test"), follow_redirects=True)
        assert b'Hi, test' in rv.data
        assert rv.status_code == 200

