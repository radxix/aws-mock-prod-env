#flask-aws-app. a random flask app that just says "hello" to your name.
#Copyright © 2019 radxix

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

from flask import Flask, render_template, request

def create_app():

    app = Flask(__name__)
    app.config.from_object('config.Config')

    @app.route("/")
    def index():
        return render_template('index.html')

    @app.route("/hi", methods=['POST'])
    def hi():
        return "Hi, " + request.form['text']
    
    return app

if __name__ == "__main__":
    app = create_app()
    app.run()
